from recon.core.module import BaseModule
import re

class Module(BaseModule):

    meta = {
        'name': 'Bing Cache Linkedin Profile and Contact Harvester',
        'author':'Brian King (@BBhacKing), based on work by @MyChickenNinja and @fullmetalcache',
        'description': 'Harvests profiles from LinkedIn by querying the Bing API cache for LinkedIn pages related to the given companies, and adds them to the \'profiles\' table. The module will then parse the resulting information to extract the user\'s full name and job title (title parsing recently improved). The user\'s full name and title are then added to the \'contacts\' table. This module does not access LinkedIn at any time.',
        'required_keys': ['bing_api'],
        'comments': (
            'Be sure to set the \'SUBDOMAINS\' option to the region your target is located in.',
            'You will get better results if you use more subdomains other than just \'www\'.',
            'Multiple subdomains can be provided in a comma separated list.',
            'Results still include some former employees, but far fewer than recent versions.',
        ),
        'query': 'SELECT DISTINCT company FROM companies WHERE company IS NOT NULL',
        'options': (
            ('limit', 0, True, 'limit total number of pages per api request (0 = unlimited)'),
            ('subdomains', None, False, 'subdomain(s) to search on LinkedIn: www, ca, uk, etc.'),
        ),
    }

    def module_run(self, companies):
        for company in companies:
            self.heading(company, level=0)
            self.get_profiles(company)
            if " " in company:
                company = company.replace(" ", "")
                self.get_profiles(company)

    def get_profiles(self, company):
        results = []
        subdomains = self.options['subdomains']
        subdomain_list = [''] if not subdomains else [x.strip()+'.' for x in subdomains.split(',')]
        for subdomain in subdomain_list:
            base_query = [
                "site:\"%slinkedin.com/in/\" \"%s\"" % (subdomain, company),
                "site:\"%slinkedin.com\" -jobs \"%s\"" % (subdomain, company),
            ]
            for query in base_query:
                results = self.search_bing_api(query, self.options['limit'])
                for result in results:
                    name     = result['name']
                    url      = result['displayUrl']
                    snippet  = result['snippet']
                    username = self.parse_username(url)
                    cache    = (name,snippet,url,company)
                    self.get_contact_info(cache)

    def parse_username(self, url):
        username = None
        username = url.split("/")[-1]
        return username

    def get_contact_info(self, cache):
        (name, snippet, url, company) = cache
        fullname, fname, mname, lname = self.parse_fullname(name)
        if fname is None or 'LinkedIn' in fullname or 'profiles' in name.lower() or re.search('^\d+$',fname): 
            # if 'name' has these, it's not a person.
            pass
        elif u'\u2013' in snippet:
            # unicode hyphen between dates here usually means no longer at company.
            # Not always, but nothing available seems more consistent than that.
            pass
        elif '/in/' not in url:
            # these are links to things that are not people.
        	pass
        elif company.lower() not in name.lower() and "at " + company.lower() not in snippet.lower():
        	# probably not there anymore. Lots of bad hits based on mentions, not employment.
        	pass
        else:
            username = self.parse_username(url)
            jobtitle = self.parse_jobtitle_from_name(company, name)
            if 'Undetermined' == jobtitle:
                jobtitle = self.parse_jobtitle_from_snippet(company, snippet)
            self.add_contacts(first_name=fname, middle_name=mname, last_name=lname, title=jobtitle)
            self.add_profiles(username=username, url=url, resource='LinkedIn', category='social')

    def parse_fullname(self, name):
        fullname = name.split(" - ")[0]
        fname, mname, lname = self.parse_name(fullname)
        return fullname, fname, mname, lname

    def parse_jobtitle_from_name(self, company, name):
        company = company[:5].lower()
        jobtitle = 'Undetermined'
        
        try:
            name, maybetitle, maybecompany = name.split(" - ")
            maybecompany = maybecompany.lower()
            if company in maybecompany:
                jobtitle = maybetitle
        except ValueError:
            print "Can't unpack w/ less than two in {0}".format(name)
            
        return jobtitle
	
    def parse_jobtitle_from_snippet(self, company, snippet):
        company = company[:5].lower()   # if no variant of company name in snippet, then no title.
        jobtitle = 'Undetermined'       # default if no title found
        chunks   = snippet.split('...') # if more than one '...' then no title or can't predict where it is
        if ' at ' in snippet and not 'See who you know' in snippet and company in snippet.lower() and len(chunks) < 3:
            if re.search('^View ', snippet):    # here we want the string after " ... " and before " at "
                m = re.search('\.{3} (?P<title>.+?) at ', snippet)
            else:                                   # here we want the string after "^$employeename. " and before " at "
                m = re.search('^[^.]+. (?P<title>.+?) at ', snippet)
            try:
                jobtitle = m.group('title')
            except AttributeError:
                pass
        return jobtitle

